<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
</head>
<body>
    <h1>{{$fullname}}</h1> <!-- เรียกค่าตัวแปรจาก Sitecontroller -->
    <p>
        <a href="https://{{$website}}">{{$website}}</a> <!-- เรียกค่าตัวแปรจาก Sitecontroller -->
    </p>
</body>
</html>
