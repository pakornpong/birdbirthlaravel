<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index() {
        $fullname = 'Akenarin Komkoon'; //การประกาศตัวแปร
        $website = 'codingthailand.com'; //การประกาศตัวแปร
        return view('about',[
            'fullname' => $fullname, //ส่งตัวแปร เพื่อไปแสดงผลview หน้า about
            'website' => $website //ส่งตัวแปร เพื่อไปแสดงผลview หน้า about
        ]); //กลับไป view หน้า about
    }
}
